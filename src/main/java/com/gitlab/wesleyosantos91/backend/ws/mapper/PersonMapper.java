package com.gitlab.wesleyosantos91.backend.ws.mapper;

import com.gitlab.wesleyosantos91.backend.ws.model.Person;
import com.gitlab.wesleyosantos91.person.model.createpersonrequest.CreatePersonRequest;
import org.mapstruct.Mapper;

/**
 * @author : wesleyosantos91
 * @Date : 14/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@Mapper(componentModel = "spring")
public interface PersonMapper {

    Person createPersonRequestToPerson(CreatePersonRequest createPersonRequest);

    com.gitlab.wesleyosantos91.person.model.person.Person personToPersonResponse(Person person);
}
