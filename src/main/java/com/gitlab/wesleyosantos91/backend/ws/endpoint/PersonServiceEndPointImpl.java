package com.gitlab.wesleyosantos91.backend.ws.endpoint;

import com.gitlab.wesleyosantos91.backend.ws.service.PersonService;
import com.gitlab.wesleyosantos91.person._interface.ApplicationFault_Exception;
import com.gitlab.wesleyosantos91.person._interface.BusinessFault_Exception;
import com.gitlab.wesleyosantos91.person._interface.PersonServiceEndPoint;
import com.gitlab.wesleyosantos91.person.model.createpersonrequest.CreatePersonRequest;
import com.gitlab.wesleyosantos91.person.model.createpersonresponse.CreatePersonResponse;
import com.gitlab.wesleyosantos91.person.model.deletepersonrequest.DeletePersonRequest;
import com.gitlab.wesleyosantos91.person.model.deletepersonresponse.DeletePersonResponse;
import com.gitlab.wesleyosantos91.person.model.readpersonrequest.ReadPersonRequest;
import com.gitlab.wesleyosantos91.person.model.readpersonresponse.ReadPersonResponse;
import com.gitlab.wesleyosantos91.person.model.updatepersonrequest.UpdatePersonRequest;
import com.gitlab.wesleyosantos91.person.model.updatepersonresponse.UpdatePersonResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author : wesleyosantos91
 * @Date : 14/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PersonServiceEndPointImpl implements PersonServiceEndPoint {

    private final PersonService service;

    @Override
    public UpdatePersonResponse updatePerson(UpdatePersonRequest personRequest) throws BusinessFault_Exception, ApplicationFault_Exception {
        return service.update(personRequest);
    }

    @Override
    public ReadPersonResponse readPerson(ReadPersonRequest personRequest) throws BusinessFault_Exception, ApplicationFault_Exception {
        return service.read(personRequest);
    }

    @Override
    public DeletePersonResponse deletePerson(DeletePersonRequest personRequest) throws BusinessFault_Exception, ApplicationFault_Exception {
        return service.delete(personRequest);
    }

    @Override
    public CreatePersonResponse createPerson(CreatePersonRequest personRequest) throws BusinessFault_Exception, ApplicationFault_Exception {
        return service.create(personRequest);
    }
}
