package com.gitlab.wesleyosantos91.backend.ws.service;

import com.gitlab.wesleyosantos91.backend.ws.mapper.PersonMapper;
import com.gitlab.wesleyosantos91.backend.ws.model.Person;
import com.gitlab.wesleyosantos91.backend.ws.repository.PersonRepository;
import com.gitlab.wesleyosantos91.person._interface.BusinessFault;
import com.gitlab.wesleyosantos91.person._interface.BusinessFault_Exception;
import com.gitlab.wesleyosantos91.person.model.createpersonrequest.CreatePersonRequest;
import com.gitlab.wesleyosantos91.person.model.createpersonresponse.CreatePersonResponse;
import com.gitlab.wesleyosantos91.person.model.deletepersonrequest.DeletePersonRequest;
import com.gitlab.wesleyosantos91.person.model.deletepersonresponse.DeletePersonResponse;
import com.gitlab.wesleyosantos91.person.model.readpersonrequest.ReadPersonRequest;
import com.gitlab.wesleyosantos91.person.model.readpersonresponse.ReadPersonResponse;
import com.gitlab.wesleyosantos91.person.model.servicestatus.ServiceStatus;
import com.gitlab.wesleyosantos91.person.model.updatepersonrequest.UpdatePersonRequest;
import com.gitlab.wesleyosantos91.person.model.updatepersonresponse.UpdatePersonResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * @author : wesleyosantos91
 * @Date : 14/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class PersonService {

    private final PersonRepository personRepository;
    private final PersonMapper personMapper;

    public CreatePersonResponse create(CreatePersonRequest personRequest) {

        Person person = personMapper.createPersonRequestToPerson(personRequest);

        Person save = personRepository.save(person);

        CreatePersonResponse createPersonResponse = new CreatePersonResponse();
        createPersonResponse.setPerson(personMapper.personToPersonResponse(save));

        createPersonResponse.setServiceStatus(getServiceStatusSucess(HttpStatus.CREATED));

        return createPersonResponse;
    }

    public DeletePersonResponse delete(DeletePersonRequest personRequest) {

        personRepository.deleteById(personRequest.getId());

        DeletePersonResponse response = new DeletePersonResponse();
        response.setServiceStatus(getServiceStatusSucess(HttpStatus.NO_CONTENT));

        return response;
    }

    public ReadPersonResponse read(ReadPersonRequest personRequest) throws BusinessFault_Exception {

        Person person = exist(personRequest.getId());

        ReadPersonResponse response = new ReadPersonResponse();
        response.setPerson(personMapper.personToPersonResponse(person));
        response.setServiceStatus(getServiceStatusSucess(HttpStatus.OK));

        return response;
    }

    public UpdatePersonResponse update(UpdatePersonRequest personRequest) throws BusinessFault_Exception {

        Person person = exist(personRequest.getId());

        BeanUtils.copyProperties(personRequest, person, "id");

        UpdatePersonResponse response = new UpdatePersonResponse();
        response.setPerson(personMapper.personToPersonResponse(person));
        response.setServiceStatus(getServiceStatusSucess(HttpStatus.OK));
        return response;
    }

    private Person exist(Long id) throws BusinessFault_Exception {
        Optional<Person> personOptional = personRepository.findById(id);

        if (!personOptional.isPresent()) {
            BusinessFault ex = new BusinessFault();
            ex.setBusinessFault("Object not foud exception");

            throw new BusinessFault_Exception(ex.getBusinessFault(), ex);
        }

        return personOptional.get();
    }

    private ServiceStatus getServiceStatusSucess(HttpStatus httpStatus) {
        ServiceStatus status = new ServiceStatus();
        status.setStatusCode(String.valueOf(httpStatus.value()));
        status.setMessage(httpStatus.getReasonPhrase());
        return status;
    }
}
