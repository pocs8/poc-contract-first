package com.gitlab.wesleyosantos91.backend.ws.util;

/**
 * @author : wesleyosantos91
 * @Date : 14/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.time.LocalDate;

public class LocalDateAdapter extends XmlAdapter<String, LocalDate> {

    @Override
    public LocalDate unmarshal(String inputDate) throws Exception {
        return LocalDate.parse(inputDate);
    }

    @Override
    public String marshal(LocalDate inputDate) throws Exception {
        return inputDate.toString();
    }

}