package com.gitlab.wesleyosantos91.backend.ws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ContractFirstApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContractFirstApplication.class, args);
    }

}
