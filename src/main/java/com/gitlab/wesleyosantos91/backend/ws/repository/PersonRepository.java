package com.gitlab.wesleyosantos91.backend.ws.repository;

import com.gitlab.wesleyosantos91.backend.ws.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author : wesleyosantos91
 * @Date : 14/05/20
 * @Contact : wesleyosantos91@gmail.com
 **/
public interface PersonRepository extends JpaRepository<Person, Long> {
}
