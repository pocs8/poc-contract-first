# POC - API FIRST

## Prova de conceito - API FIRST

> O foco do estilo contract-first são as mensagens XML, deixando o código da linguagem de implementação desacoplado do contrato do serviço.

# Tecnologias
- Java 8
- Spring Boot 2.2.7.RELEASE
    - spring-boot-starter-web
    - spring-boot-starter-data-jpa
    - spring-boot-devtools
    - spring-boot-starter-web-services
- Flywaydb
- Lombok
- H2
- Apache CXF
- Mapstruct jdk8
- Tomcat (Embedded no Spring Boot)
- Git

# Execução

A execução das aplicações são feitas através do de um comando Maven que envoca a inicialização do Spring Boot.

- Scripts
    - ```./mvnw clean spring-boot:run```
    
# Utilização

- http://localhost:8080/soap-api

- http://localhost:8080/soap-api/service/persons?wsdl


